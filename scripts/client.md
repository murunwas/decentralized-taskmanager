# Decentralized task manager client

## create app
```bash
# create sveltekit app
npm init svelte@next .

# instal tailwind css
npx svelte-add@latest tailwindcss 

# install dependencies
npm install
```

> Setup

```bash
# Install etherjs
npm i ethers
```

## TODO list
### User Interface
- [x] layout
- [x] Add task form
- [x] Task list component
- [x] Task componemt

### metamask Controller
- [ ] check if metamask is installed
- [ ] check if chainid (network) is correct.
- [ ] check if account is locked

### Ethers Provider
- [ ] connect to blockchain
- [ ] get signers
- [ ] get smart contract

### Taskmanager controller
- [ ] get smart contract name
- [ ] get user balance
- [ ] get current address
- [ ] get all user tasks
- [ ] create new task
- [ ] update task
- [ ] delete task

### helper Controller
- [ ] alerts
- [ ] notifications
- [ ] prompts






