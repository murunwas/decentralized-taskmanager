# decentralized-taskmanager



> Task manager  (TDD - Test Driven Development) and (CQRS - Command and Query Responsibility Segregation)

### Reads
- [x] Task manager must have a name
- [x] Task manager must have an owner
- [x] Task manager must have a private collection to store tasks
- [x] user can view their tasks only
- [x] get task by index

### Writes
- [x] User can add one ore more Tasks to their collections only
- [x] must not allow empty task to be added 
- [x] user can delete their task only
- [x] user can update their task only

# Editing this README

