const fs  =require("fs")
const path = require("path")
const artifacts = require("./artifacts/contracts/TaskManager.sol/TaskManager.json")


function copy(address) {
    const data ={
        address,
        abi:artifacts.abi
    }
    const clientPath=path.join(__dirname,"..","client","src","lib","contracts")
    if (!fs.existsSync(clientPath)) {
        fs.mkdirSync(clientPath)
    }
    fs.writeFileSync(`${clientPath}/taskmanager.json`,JSON.stringify(data,null,2))
}

module.exports = copy;