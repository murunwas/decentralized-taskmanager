require("dotenv").config();

const { PRIVATE_KEY, PRIVATE_RPC_URL} = process.env;


module.exports = {
    mumbai: {
        url: PRIVATE_RPC_URL,
        accounts: [PRIVATE_KEY]
    } 
}