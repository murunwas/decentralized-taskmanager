//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.4;

contract TaskManager {
    string public name = "Decentralised Task manager";
    address public owner;

    mapping(address => Task[]) private collection;

    //Structs
    struct Task {
        string text;
        bool completed;
        uint256 createdTime;
    }

    constructor() {
        owner = msg.sender;
    }

    //modifiers
    modifier validateText(string memory _text) {
        require(bytes(_text).length != 0, "text is required");
        _;
    }

        modifier taskNotFound(uint256 _taskIndex) {
        require(_taskIndex < collection[msg.sender].length, "Task not found");
        _;
    }

    //Events
    event TaskAdded(address owner, uint256 createdTime);
    event TaskDeleted(address owner, uint256 createdTime);
    event TaskUpdated(address owner, uint256 createdTime);

    //Methods
    function getTaskCount() external view returns (uint256) {
        return collection[msg.sender].length;
    }

    function addTask(string memory _text) external validateText(_text) {
        uint256 createdTime = block.timestamp;
        collection[msg.sender].push(
            Task({text: _text, completed: false, createdTime: createdTime})
        );

        emit TaskAdded(msg.sender, createdTime);
    }

    function getAll() external view returns (Task[] memory) {
        return collection[msg.sender];
    }

    function getTaskByIndex(uint256 _taskIndex)
        external
        view
        taskNotFound(_taskIndex)
        returns (Task memory)
    {
        return collection[msg.sender][_taskIndex];
    }


    function deleteTaskByIndex(uint256 _taskIndex) external taskNotFound(_taskIndex){
    collection[msg.sender][_taskIndex] = collection[msg.sender][collection[msg.sender].length-1];
    collection[msg.sender].pop();
    emit TaskDeleted(msg.sender,block.timestamp);
    }

    function updateStatus(uint256 _taskIndex, bool _status) external  taskNotFound(_taskIndex){
        collection[msg.sender][_taskIndex].completed = _status;
        emit TaskUpdated(msg.sender,block.timestamp);
    }
}
