const { solidity } = require("ethereum-waffle")
const { expect } = require("chai");
const assertArrays = require('chai-arrays');
const { ethers } = require("hardhat");

let chai = require("chai");
chai.use(assertArrays);
chai.use(solidity);

describe("taskManager", function () {

    let taskManager;
    let owner;
    let addr1;
    const NAME = "Decentralised Task manager";


    before(async function () {
        const taskManagerContract = await ethers.getContractFactory("TaskManager");

        [owner, addr1] = await ethers.getSigners();
        taskManager = await taskManagerContract.deploy();
        await taskManager.deployed();
    })


    describe("Deployment", async function () {
        it("should have name", async () => {
            expect(await taskManager.name()).to.equal(NAME)
        })

        it("should have an owner", async () => {
            const results = await taskManager.owner();
            expect(results).to.equal(owner.address)
        })
    });

    describe("Transactions", async function () {
        it("should have no tasks", async () => {
            const count = await taskManager.getTaskCount();
            expect(count).to.equal(0)
        })

        it("should throw exception if text is empty", async () => {
            const textMessage = "";
            await expect(taskManager.addTask(textMessage)).to.be.revertedWith("text is required")
        })

        it("should add task", async () => {
            const textMessage = "first task";
            const tx = await taskManager.addTask(textMessage);
            const reciept = await tx.wait();
            const count = await taskManager.getTaskCount();
            expect(count).to.equal(1)
            const TaskAdded = reciept.events?.filter(e => e.event == "TaskAdded");
            expect(TaskAdded).to.be.array()
            expect(TaskAdded).to.be.ofSize(1);

        })

        it("should get all user tasks", async () => {
            const tasks = await taskManager.getAll();
            expect(tasks).to.be.array()
            expect(tasks).to.be.ofSize(1);
        })

        it("should get task by id", async () => {
            const index = 0;
            const textMessage = "first task";
            const task = await taskManager.getTaskByIndex(index);
            expect(task.text).to.equal(textMessage)
            expect(task.completed).to.equal(false)
        })

        it("should throw exception task is not found", async () => {
            const index = 1;
            await expect(taskManager.getTaskByIndex(index)).to.be.revertedWith("Task not found")
        })

        it("should update task", async () => {
            const index = 0;
            const completed=true;
            const tx = await taskManager.updateStatus(index,completed);
            const receipt = await tx.wait();

            const task = await taskManager.getTaskByIndex(index);
            expect(task.completed).to.equal(true)
        })

        it("should have remove task by index", async () => {
            const index = 0;
            const tx = await taskManager.deleteTaskByIndex(index);
            const receipt = await tx.wait();

            const taskDeleted = receipt.events?.filter(ev => ev.event == "TaskDeleted");
            expect(taskDeleted).to.be.array();
            expect(taskDeleted).to.be.ofSize(1)

            const count = await taskManager.getTaskCount();
            expect(count).to.equal(0)
        })
    })

});