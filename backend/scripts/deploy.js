const hre = require("hardhat");
const copy = require("../copy")

async function main() {

  const TaskManager = await hre.ethers.getContractFactory("TaskManager");
  const taskManager = await TaskManager.deploy();

  await taskManager.deployed();

  console.log("taskManager deployed to:", taskManager.address);
  copy(taskManager.address);
}


main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
